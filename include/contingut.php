<div id="exercicis">
	<div id="exercici01">
   <h2>Exercici 01</h2>
	 <p>
		 <span class="destacat">Enunciat:</span><br/>
		 Para ello el usuario debe escribir el nombre y seleccionar el fichero según quiera buscar frutos o árboles. Además eligirá el color de fondo con el que desea ver esa información, que puede ser rojo, azul o verde. 

		<br/>Para implementarlo utiliza el fichero processaExercici01.php para:<br/>
			Obtener valores del formulario.<br/>
			Obtener linia correspondiente del fichero elegido.<br/>
			Acceder a blau.php, verd.php o roig.php en función del color elegido. En este fichero se tiene que mostrar el nombre del fruto o del árbol en mayúscular y el texto de la línea del fichero, sin los dos puntos :<br/>
	 </p>
   <form id="form_exercici01" name="form_exercici01" method="POST" action="./include/processaExercici01.php">
      <ul>
				<li>
            <span>Nom:</span>
            <span><input id="nom" name="nom" type="text" required /></span>
         </li>
				 <li>
						<span>Color:</span>
            <span>
               <select id="colorExercici01" name="color">
                  <option value="">-- Tria un color --</option>
                  <option value="roig">Roig</option>
                  <option value="verd">Verd</option>
                  <option value="blau">Blau</option>
               </select>
            </span>
         </li>
         <li>
            <!--<span>Número:</span>
            <span><input id="num" name="num" type="number" max="5" min="1" required /></span>-->
         </li>
				 <li>
						<span>Fitxer:</span>
            <span>
               <select id="fitxerExercici01" name="fitxer">
                  <option value="">-- Tria un fitxer --</option>
                  <option value="fitxer01">Fitxer Fruita</option>
                  <option value="fitxer02">Fitxer Arbres</option>
               </select>
            </span>
         </li>
				 <li>
            <span><input id="envia" name="envia" type="submit" value="Envia Exercici 01"/></span>
         </li>
			</ul>
		</form>
	</div><!--final exercici 01-->
	
	
	<div id="exercici02">
		<h2>Exercici 02</h2>
		<p>
		 <span class="destacat">Enunciat:</span><br/>
		Obtener los parámetros que contiene el enlace.<br/>
Crear una función a la que le pasamos estos parámetros. La función tiene que:<br/>
Mostrar la imagen que tiene el nombre indicado en el primer parámetro.<br/>
Lo tiene que mostrar tantas veces como indica el segundo parámetro.<br/>
Añadir a fitxerExercici02.txt una nueva línea con el nombre de la imagen y el número de veces que se ha impreso, separados por :<br/>
	 </p>
	 <figure class="imatge">
		<a href="./include/processaExercici02.php?imatge=pera&num=3">		
			<img src="img/pera.jpg" alt="Pera" class="imatgeExercici02" />
		</a>
		<a href="./include/processaExercici02.php?imatge=magrana&num=2">
			<img src="img/magrana.jpg" alt="Magrana" class="imatgeExercici02" />
		</a>
		<a href="./include/processaExercici02.php?imatge=taronja&num=4">
			<img src="img/taronja.jpg" alt="Taronja" class="imatgeExercici02" />
		</a>
	 </figure>
	</div> <!--final exercici 02-->
	
	
</div>

